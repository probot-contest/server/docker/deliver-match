FROM alpine AS build
RUN apk add --no-cache g++
COPY *.cpp *.h ./
RUN g++ --static --std=c++11 *.cpp -o main

FROM docker
RUN apk add --no-cache bash
WORKDIR /workdir
#RUN apk add --no-cache strace
COPY deliver.sh .
CMD ./deliver.sh
COPY --from=build main .
