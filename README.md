# gameeval

Contains three moduls: `game`, `eval` and `io`.

Eval is the main module, which uses io to communicate with the player's bot, and the game to modify or query current state.
