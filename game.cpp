#include <cassert>
#include <queue>
#include "game.h"
#include "player.h"
#include "zone.h"
#include "actionlist.h"

using std::pair;

void Game::setPlayer(int i, Player* p) {
  assert(i == 1 || i == 2);
  if (i == 1) player1 = p;
  if (i == 2) player2 = p;
}

Game::Game() : player1(NULL), player2(NULL), turn_number(0),
  player1_loses(false), player2_loses(false) { }

void Game::genBases() {
  int base1id = rand()%getNumberOfZones();
  int base2id;
  do {
    base2id = rand()%getNumberOfZones();
  } while (base1id == base2id);

  Zone* base1 = getZone(base1id);
  Zone* base2 = getZone(base2id);

  base1->setOwner(player1);
  base1->setUnits(player1, 10);

  base2->setOwner(player2);
  base2->setUnits(player2, 10);
}

void Game::init() {
  genMap(20, 20, 40);
  genLinks();

  genBases();

  // this must be after any initialization:
  player1->broadcastInit(this);
  player2->broadcastInit(this);
}

void Game::genLinks() {
  assert(links.empty());
  for (int i = 1; i <= N; i++) {
    for (int j = 1; j <= M; j++) {
      if (map[i][j] == NULL)
        continue;
      Zone* z1 = map[i][j];
      if (map[i][j+1] != NULL) {
        Zone* z2 = map[i][j+1];
        links.push_back({z1,z2});
        z1->addAdjacentZone(z2);
        z2->addAdjacentZone(z1);
      }
      if (map[i+1][j] != NULL) {
        Zone* z2 = map[i+1][j];
        links.push_back({z1,z2});
        z1->addAdjacentZone(z2);
        z2->addAdjacentZone(z1);
      }
    }
  }
}

int getRandomProduction() {
  if (rand()%2)
    return 0;
  if (rand()%2)
    return rand()%3;
  return rand()%7;
}

void Game::genMap(int N, int M, int Power) {
  this->N = N;
  this->M = M;
  assert(Power>0);
  map.resize(N+2);
  for (int i = 0; i < N+2; i++) {
    map[i].resize(M+2);
    for (int j = 0; j < M+2; j++)
      map[i][j] = NULL;
  }

  assert(zones.size() == 0);
  zones.reserve((N+2)*(M+2));
  std::queue<std::pair<std::pair<int,int>, int>> K;
  auto fillZone = [&](int x, int y, int power){
    assert(x > 0 && y > 0 && x <= N && y <= M && map[x][y] == NULL);
    Zone* zone = new Zone(zones.size(), getRandomProduction());
    zones.push_back(zone);
    map[x][y] = zone;
    K.push({{x, y}, power});
  };

  fillZone(1+N/2, 1+M/2, Power);
  while (!K.empty()) {
    using std::tie;
    int x = K.front().first.first;
    int y = K.front().first.second;
    int k = K.front().second;
    K.pop();
    assert(k>=0);
    auto propagate = [&](int x, int y){
      if (x > 0 && y > 0 && x <= N && y <= M && map[x][y] == NULL) {
        fillZone(x, y, Power - rand()%5);
      }
    };
    propagate(x, y+1);
    propagate(x, y-1);
    propagate(x+1, y);
    propagate(x-1, y);
  }
}

bool Game::ended() {
  if (player1->getBase()->getOwner() != player1)
    return true;
  if (player2->getBase()->getOwner() != player2)
    return true;
  // TODO
  return turn_number != 501;
}

/** Multiple game-ending condition can be true, so we must be careful
  * If one base is occupied, then that must determine the win condition
  */
std::pair<int, int> Game::getScore() {
  std::pair<int, int> win1 = {2,0}, tie = {1,1}, win2 = {0,2};
  // TODO NOT FAIR! If both player loses by e.g. invalid output, then nobody will notice it
  if (player1_loses) return win2;
  if (player2_loses) return win1;

  if (player1->getBase()->getOwner() != player1) {
    if (player2->getBase()->getOwner() != player2)
      return tie;
    return win2;
  } else if (player2->getBase()->getOwner() != player2)
    return win1;

  assert(turn_number == 501);

  int zone1 = player1->getNumberOfZones();
  int zone2 = player2->getNumberOfZones();
  if (zone1 < zone2) return win2;
  if (zone2 > zone1) return win1;
  return tie;
}

void Game::step(const ActionList& al1, const ActionList& al2) {
  // TODO
  turn_number++;
}

void Game::newTurn() {
  // give them the data for the turn
  player1->broadcastData(this);
  player1->resetTimer();
  player2->broadcastData(this);
  player2->resetTimer();

  // Get moves, check time-out, check input, LOG?
  // TODO getActions should terminate on time-out
  // TODO i/o should be buffered so the player does not have to wait for us to get the answers
  ActionList al1 = player1->getActions(this);
  if (player1->getTimer() > 100) {
    // TODO The evaluator will not be fair if both time-out!
    // TODO throw error, player 2 wins
    // TODO log...
    player1_loses = true;
    return;
  }
  if (!valid(player1, al1)) {
    // TODO throw error, player 2 wins
    player1_loses = true;
    return;
  }
  ActionList al2 = player2->getActions(this);
  if (player2->getTimer() > 100) {
    // TODO throw error, player 1 wins
    player2_loses = true;
    return;
  }
  if (!valid(player2, al2)) {
    // TODO throw error, player 1 wins
    player1_loses = true;
    return;
  }

  // do the moves
  step(al1, al2);
}

bool Game::valid(Player* player, const ActionList& actionlist) {
  // TODO rewrite actionlist to normal
  // SHIIIIIIIT asserts that actionlist doesn't iterate over the same `from` item twice
  std::unordered_map<Zone*, int> unitPerZone; // XXX needs init?
  if (!actionlist.isValid())
    return false;
  for (Action a: actionlist) {
    unitPerZone[a.from] += a.number;
    if (a.number > 1000) return false; // prevent overflow at its root
    if (!a.from->adjacent(a.to))
      return false;
  }
  for (auto it: unitPerZone) {
    if (it.second > it.first->getPods(player))
      return false;
  }
  return true;
}

