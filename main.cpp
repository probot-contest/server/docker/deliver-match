// Players communicate with the game, Io generates two players with which we can communicate
#include "game.h"
#include "io.h"

int main() {
  Game game;
  Io io;
  game.setPlayer(1, io.genPlayer(1));
  game.setPlayer(2, io.genPlayer(2));

  game.init();
  while (!game.ended()) {
    game.newTurn();
  }
  io.dumpResult(game.getScore());
}
