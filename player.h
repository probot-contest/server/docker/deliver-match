#ifndef _PLAYER_H_
#define _PLAYER_H_

#include <unordered_set>
#include <chrono>
#include "actionlist.h"

class Zone;
class Game;

class Player {
  Zone* base;
  std::unordered_set<Zone*> owned_zones;
  std::chrono::steady_clock::time_point tp_last_reset = std::chrono::steady_clock::now();
  int id;
public:
  Player(int id) : id(id) {}
  virtual void broadcastInit(Game* g)=0;
  virtual void broadcastData(Game* g)=0;
  virtual BareActionList getActions()=0;
  ActionList getActions(Game* g) { return getActions().applyGame(g); }
  virtual ~Player() {}

  int getId() { return id; }

  Zone* getBase() {
    return base;
  }
  int getNumberOfZones() {
    return owned_zones.size();
  }

  // resets the timer
  void resetTimer() {
    tp_last_reset = std::chrono::steady_clock::now();
  }
  // returns in millisec the time to respond
  int getTimer() {
    auto now = std::chrono::steady_clock::now();
    return std::chrono::duration_cast<std::chrono::milliseconds>(now - tp_last_reset).count();
  }
};

#endif
