#ifndef _ZONE_H_
#define _ZONE_H_

#include <cassert>
#include <vector>
#include <unordered_set>

#include "player.h"

class Zone {
  Player* owner;
  int pod1, pod2;
  int id;
  int production;
  std::unordered_set<Zone*> adjacent_zones;
public:
  Zone(int id, int production) : owner(NULL), pod1(0), pod2(0), id(id), production(production) {}
  Player* getOwner() { return owner; }

  // must be called only on initializing
  void addAdjacentZone(Zone* zone) {
    adjacent_zones.insert(zone);
  }

  bool adjacent(Zone* zone) {
    return adjacent_zones.find(zone) != adjacent_zones.end();
  }
  
  int getPods(Player* player) {
    switch(player->getId()) {
      case 1: return pod1;
      case 2: return pod2;
      default: assert(0);
    }
  }

  void setOwner(Player* p) { owner = p; }
  void setUnits(Player* p, int pod) {
    switch(p->getId()) {
      case 1: pod1 = pod; break;
      case 2: pod2 = pod; break;
      default: assert(0);
    }
  }

  int getProduction() { return production; }
  
  int getId() { return id; }
};

#endif
